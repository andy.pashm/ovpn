from django.urls import path
from . import views
from .views import MyTokenObtainPairView
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

app_name = 'api'

urlpatterns = [
    path('', views.getRoutes),
    path('token/', MyTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('home/', views.HomeView.as_view(), name='home'),
    path('profile/', views.profile_api, name='profile_api'),
    path('superadmin/', views.superadmin_api, name='superadmin_api'),
    path('account/create/', views.create_account_api, name='create_account_api'),
    path('account/charge/', views.charge_account_api, name='charge_account_api'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
