import os

import requests
from django.core.files import File

from account.models import Account, Action
from keys import telegram_bot_token, xpanel_token

current_dir = os.getcwd()


def document_sender(chat_id, file, caption):
    apiToken = telegram_bot_token
    apiURL = f'https://api.telegram.org/bot{apiToken}/sendDocument'
    files = {'document': open(file, 'rb')}
    data = {'chat_id': chat_id, 'parse_mode': 'HTML', 'caption': caption}
    r = requests.post(apiURL, data=data, files=files, stream=True)
    return r.json()


def message_sender(message, chat_id):
    apiToken = telegram_bot_token
    apiURL = f'https://api.telegram.org/bot{apiToken}/sendMessage'
    data = {'chat_id': chat_id, 'text': message}
    r = requests.post(apiURL, data=data)
    return r.json()


use_ssh_creation = False


def create_ssh_config(username, password, expdate):
    if use_ssh_creation:
        token = xpanel_token
        url = "https://cofee.fdlock.xyz:1978/api/adduser"

        j = {'token': token, 'username': username, 'password': password, 'multiuser': '1', 'traffic': '50',
             'type_traffic': 'gb', 'expdate': expdate}
        x = requests.post(url, json=j)
        return x
    else:
        pass


def activate_ssh_user(username):
    token = xpanel_token
    url = "https://cofee.fdlock.xyz:1978/api/active"

    j = {'token': token, 'username': username}
    x = requests.post(url, json=j)
    return x


def deactivate_ssh_user(username):
    token = xpanel_token
    url = "https://cofee.fdlock.xyz:1978/api/deactive"

    j = {'token': token, 'username': username}
    x = requests.post(url, json=j)
    return x


def account_generator(profile, server_ip, account_name):
    none_name = ''
    for i in os.listdir('{}/cli/{}'.format(current_dir, server_ip)):
        if i.startswith('cli_'):
            none_name = i
            break

    if none_name != '':
        global pas
        pas = ''
        with open('{}/cli/{}/pass.txt'.format(current_dir, server_ip), 'r') as f:
            lines = f.readlines()
            for line in lines:
                if line.startswith(none_name):
                    pas = line.split(' : ')[1]

        os.rename('{}/cli/{}/{}'.format(current_dir, server_ip, none_name),
                  '{}/cli/{}/{}.ovpn'.format(current_dir, server_ip, account_name))
        with open('{}/cli/{}/{}.ovpn'.format(current_dir, server_ip, account_name), 'rb') as f:
            ovpn_file = File(f)
            ovpn_file = File(f, name=os.path.basename('{}/cli/{}/{}.ovpn'.format(current_dir, server_ip, account_name)))
            account = Account(name=account_name, password=pas, file=ovpn_file, server=profile.server,
                              cli_name=none_name.split('.')[0], leader=profile)
            account.save()

        create_ssh_config(account.name, account.password, account.date_end.strftime("%Y-%m-%d"))
        if profile.server.ssh_status == False:
            deactivate_ssh_user(account.name)

        document_sender(profile.chat_id, '{}/cli/{}/{}.ovpn'.format(current_dir, server_ip, account_name), pas)
        m = "Host: cofee.fdlock.xyz\nport: 22\nusername: {}\npassword: {}".format(account.name, account.password)
        message_sender(m, profile.chat_id)
        os.remove('{}/cli/{}/{}.ovpn'.format(current_dir, server_ip, account_name))

        action = Action(leader=profile, action=0, account=account)
        action.save()

        message = '{}: create > {}'.format(profile, account)
        message_sender(message, '515098162')

        return True
    else:
        return False
